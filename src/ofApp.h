#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	
	ofVideoGrabber cam;
	ofxCv::RunningBackground background;
	ofImage thresholded;

	ofLight pointLight1, pointLight2;
	ofMaterial material;
	ofCamera camera;
    
    ofxPanel gui;
    ofParameter<bool> resetBackground;
	ofParameter<float> learningTime, thresholdValue;
	float boxAngle;

	vector<ofBoxPrimitive> boxes;
};
 