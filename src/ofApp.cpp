#include "ofApp.h"

using namespace ofxCv;
using namespace cv;

void ofApp::setup() {
	cam.setup(800, 600);

	for (int x = 10; x < 800; x += 20)
	{
		for (int y = 10; y < 600; y += 20) {
			ofBoxPrimitive box;
			box.setPosition(x, y, 0);
			box.set(18);
			box.setOrientation(glm::vec3(90, 0, 0));
			boxes.push_back(box);
		}
	}
    
    gui.setup();
    gui.add(resetBackground.set("Reset Background", false));
    gui.add(learningTime.set("Learning Time", 10, 0, 30));
    gui.add(thresholdValue.set("Threshold Value", 25, 0, 255));
	//gui.add(boxAngle.set("Box Angle", 90, 90, 140));

	pointLight1.setDiffuseColor(ofFloatColor(.85, .85, .55));
	pointLight1.setSpecularColor(ofFloatColor(1.f, 1.f, 1.f));
	pointLight1.setPosition(400, -500, 10);

	pointLight2.setDiffuseColor(ofFloatColor(.45, .55, .7));
	pointLight2.setSpecularColor(ofFloatColor(1.f, 1.f, 1.f));
	pointLight2.setPosition(400, 1100, 100);

	camera.setOrientation(glm::vec3(0, 0, 180));
	camera.setPosition(400, 300, 430);

	// shininess is a value between 0 - 128, 128 being the most shiny //
	material.setShininess(250);
	// the light highlight of the material //
	material.setSpecularColor(ofColor(255, 255, 255, 255));

	ofEnableDepthTest();
}

void ofApp::update() {
	cam.update();
    if(resetBackground) {
        background.reset();
        resetBackground = false;
    }
	if(cam.isFrameNew()) {
        background.setLearningTime(learningTime);
        background.setThresholdValue(thresholdValue);
		background.update(cam, thresholded);
		thresholded.update();
	}
}

void ofApp::draw() {
	//cam.draw(0, 0);
	camera.begin();
	ofEnableLighting();
	pointLight1.enable();
	pointLight2.enable();

	material.begin();
	for (auto&& box: boxes)
	{
		glm::vec3 pos = box.getPosition();
		glm::vec3 rot = box.getOrientationEulerDeg();
		boxAngle = rot.x;
		//ofSetColor(thresholded.getColor(pos.x, pos.y));
		if (thresholded.getColor(pos.x, pos.y).r > 180 && boxAngle < 140) {
			box.rotateDeg(5, glm::vec3(1, 0, 0));
		}else if (boxAngle > 90)box.rotateDeg(-1.0, glm::vec3(1, 0, 0));
		ofSetColor(0);
		//box.drawWireframe();
		box.draw();
	}
	material.end();
	camera.end();

	ofSetColor(255);
   // if(thresholded.isAllocated()) {
   //     thresholded.draw(640, 0);
   // }
    //gui.draw();
}
